/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import data.DataPacket;
import data.Room;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class ClientHandler extends Thread {

    private String playerName;
    private Socket client;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private Server server;
    private Room room;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public ClientHandler(Socket client, Server server) {
        try {
            this.client = client;
            this.server = server;
            oos = new ObjectOutputStream(client.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    @Override
    public void run() {
        try {
            //vòng lặp vô hạn để nhận dữ liệu gửi từ client
            //các thao tác xử lý game
            while (true) {
                ois = new ObjectInputStream(client.getInputStream());
                DataPacket data = (DataPacket) ois.readObject();
                this.handleData(data);
            }
        } catch (Exception e) {
        } finally {
            System.out.println(this.playerName + " đã ngắt kết nối");
            server.removeClient(this);
        }
    }

    public void setRole(String role) {
        try {
            System.out.println(this.playerName + ": đặt vai trò");
            DataPacket<String> response = new DataPacket("SET_ROLE", role);
            oos.writeObject(response);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gameStart(ArrayList<Integer> listXEnemy) {
        try {
            System.out.println(this.playerName + ": trò chơi bắt đầu");
            DataPacket<ArrayList<Integer>> response = new DataPacket("GAME_START", listXEnemy);
            oos.writeObject(response);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendAction(String role, int action) {
        try {
            String actionType = "";
            if (action == KeyEvent.VK_LEFT || action == KeyEvent.VK_RIGHT || action == 0) {
                actionType = role + "_DIRECTION";
            }
            DataPacket<Integer> response = new DataPacket(actionType, action);
            oos.writeObject(response);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendRoom() {
        try {
            System.out.println("gui room da tao cho " + this.playerName);
            DataPacket<String> response = new DataPacket("RESPONSELISTROOM", server.getListRoom());
            oos.writeObject(response);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendFire(String role) {
        try {
            DataPacket<String> response = new DataPacket(role + "_FIRE", "");
            oos.writeObject(response);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendLostHP() {
        try {
            DataPacket<String> response = new DataPacket("LOST_HP", "");
            oos.writeObject(response);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void handleData(DataPacket data) {
        switch (data.getAction()) {
            case "LOGIN": {
                this.playerName = (String) data.getData();
                System.out.println(this.playerName + " logged in");
                break;
            }
            case "REQUESTLISTROOM": {
                System.out.println(this.playerName + " request list room");
                try {

                    DataPacket<String> response = new DataPacket("RESPONSELISTROOM", server.getListRoom());
                    oos.writeObject(response);
                } catch (Exception e) {
                }
                break;
            }
            case "CREATE_ROOM": {
                System.out.println(this.playerName + " create room");
                server.createRoom(this);
                break;
            }
            case "JOIN_ROOM": {
                String selectedRoom = (String) data.getData();
                System.out.println(this.playerName + " want to join room " + selectedRoom);
                server.joinRoom(selectedRoom, this.playerName);
                break;
            }
            case "HOST_ACTION": {
                int action = Integer.parseInt((String) data.getData());
                room.sendAction("HOST", action);
                break;
            }
            case "GUEST_ACTION": {
                int action = Integer.parseInt((String) data.getData());
                room.sendAction("GUEST", action);
                break;
            }
            case "HOST_FIRE": {
                room.sendFire("HOST");
                break;
            }
            case "GUEST_FIRE": {
                room.sendFire("GUEST");
                break;
            }
            case "LOST_HP": {
                room.sendLostHP();
                break;
            }
        }
    }

}
