/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import data.Room;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author admin
 */
public class Server {

    private final int PORT = 7778;
    private final ExecutorService pool = Executors.newFixedThreadPool(10);
    private ArrayList<ClientHandler> listClient = new ArrayList<>();
    private ArrayList<Room> listRoom = new ArrayList<>();

    public static void main(String[] args) {
        new Server().runServer();
    }

    private void runServer() {
        try {
            ServerSocket server = new ServerSocket(PORT);
            System.out.println("Máy chủ khởi chạy...");
            System.out.println("IP máy chủ: " + InetAddress.getLocalHost().getHostAddress());
            //vòng lặp vô hạn để chờ client kết nối lên server
            while (true) {
                Socket client = server.accept();
                ClientHandler clientThread = new ClientHandler(client, this);
                listClient.add(clientThread);
                pool.execute(clientThread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkDuplicate(String name) {
        boolean flag = false;
        for (ClientHandler client : listClient) {
            if (client.getPlayerName().equals(name)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public void createRoom(ClientHandler client) {
        Room room = new Room(client.getPlayerName());
        listRoom.add(room);
        listClient.forEach((c) -> {
            c.sendRoom();
        });
    }

    public void joinRoom(String host, String guest) {
        for (Room room : listRoom) {
            if (room.getHost().equals(guest)) {
                room.setStarted(true);
            }
            if (room.getHost().equals(host)) {
                room.setGuest(guest);
                for (ClientHandler client : listClient) {
                    if (client.getPlayerName().equals(host)) {
                        room.setHostHandler(client);
                    }
                    if (client.getPlayerName().equals(guest)) {
                        room.setClientHandler(client);
                    }
                }
                room.gameStart();
                room.setStarted(true);
            }
            listClient.forEach((c) -> {
                c.sendRoom();
            });
        }
    }

    public String getListRoom() {
        String rooms = ";";
        for (Room r : listRoom) {
            if (!r.isStarted()) {
                rooms = rooms + r.getHost() + ";";
            }
        }
        return rooms;
    }

    public void removeClient(ClientHandler client) {
        for (Room room : listRoom) {
            if (room.getHost().equals(client.getPlayerName())) {
                listRoom.remove(room);
            }
        }
        this.listClient.remove(client);
    }
}
