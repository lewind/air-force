/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import server.ClientHandler;

/**
 *
 * @author Admin
 */
public class Room implements Serializable {

    private String host;
    private ClientHandler hostHandler;
    private String guest;
    private ClientHandler clientHandler;
    private ArrayList<Integer> listXEnemy = new ArrayList<>();
    private boolean started;

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public ClientHandler getHostHandler() {
        return hostHandler;
    }

    public void setHostHandler(ClientHandler hostHandler) {
        this.hostHandler = hostHandler;
    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    public void setClientHandler(ClientHandler clientHandler) {
        this.clientHandler = clientHandler;
    }

    public Room(String host) {
        this.host = host;
        started = false;
    }

    public String getHost() {
        return host;
    }

    public String getGuest() {
        return guest;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    @Override
    public String toString() {
        return host;
    }

    private void initEnemies(int count) {
        for (int i = 0; i < count; i++) {
            int x = ThreadLocalRandom.current().nextInt(0, 700);
            listXEnemy.add(x);
        }
    }

    public void gameStart() {
        initEnemies(20);
        hostHandler.setRoom(this);
        clientHandler.setRoom(this);

        hostHandler.setRole("HOST");
        clientHandler.setRole("GUEST");

        hostHandler.gameStart(listXEnemy);
        clientHandler.gameStart(listXEnemy);
    }

    public void sendAction(String role, int action) {
        hostHandler.sendAction(role, action);
        clientHandler.sendAction(role, action);
    }

    public void sendFire(String role) {
        hostHandler.sendFire(role);
        clientHandler.sendFire(role);
    }

    public void sendLostHP() {
        hostHandler.sendLostHP();
        clientHandler.sendLostHP();
    }
}
