/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author admin
 */
public class Bullet extends JLabel {

//    private Timer timer = new Timer(1, new ActionListener() {
//        @Override
//        public void actionPerformed(ActionEvent e) {
//            if (e.getSource() == timer) {
//                if (y < 0) {
//                    timer.stop();
//                    destroyed = true;
//                    setVisible(false);
//                } else {
//                    setBounds(x, y -= 1, 6, 10);
//                }
//
//            }
//        }
//    });
    private int x;
    private int y;
    private boolean destroyed;

    public Bullet(int x, int y) {
        this.x = x;
        this.y = y;
        destroyed = false;
        setBounds(x, y, 6, 10);
        setIcon(new ImageIcon("images/bullet6x10.png"));
        //timer.start();
    }

    public void move() {
        if (y < 0) {
            destroyed = true;
            setVisible(false);
        } else {
            y -= 2;
            setBounds(x, y, 6, 10);
        }
    }

    @Override
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void explode() {
        setVisible(false);
        destroyed = true;
        //timer.stop();
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean isDestroyed) {
        this.destroyed = isDestroyed;
    }

}
