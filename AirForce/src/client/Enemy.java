/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author Admin
 */
public class Enemy extends JLabel {

    Timer timer = new Timer(5, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == timer) {
                if (y < 500) {
                    setBounds(x, y += speed, SIZE, SIZE);
                } else {
                    timer.stop();
                    destroyed = true;
                    setVisible(false);
                }
            }
        }
    });
    private final int SIZE = 100;
    private int x;
    private int y;
    private int level;
    private int speed;
    private boolean destroyed;
    private boolean started;

    public Enemy(int x, int speed) {
        this.x = x;
        this.y = -50;
        this.speed = speed;
        destroyed = false;
        started = false;
        setBounds(x, y, SIZE, SIZE);
        setIcon(new ImageIcon("images/fighter3100x100.png"));
    }

    public void move() {
        if (y > 600) {
            destroyed = true;
            setVisible(false);
        } else {
            y += speed;
            setBounds(x, y, SIZE, SIZE);
        }
        //timer.start();
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void explode() {
        destroyed = true;
        timer.stop();
        setVisible(false);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }
}
