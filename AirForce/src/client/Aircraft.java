/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author admin
 */
public class Aircraft extends JLabel {

    private final int SIZE = 100;
    private int x;
    private int y;
    private int speed = 2;
    private boolean destroyed;

    public Aircraft(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.destroyed = false;
        setBounds(x, y, SIZE, SIZE);
        if (type == 1) {
            setIcon(new ImageIcon("images/fighter2100x100.png"));
        } else {
            setIcon(new ImageIcon("images/fighter1100x100.png"));
        }

    }

    public void explode() {
        destroyed = true;
        setVisible(false);
    }

    public void move(int direction) {
        switch (direction) {
            case KeyEvent.VK_LEFT: {
                if (x > 0) {
                    x -= speed;
                    setBounds(x, y, SIZE, SIZE);
                }
                break;
            }
            case KeyEvent.VK_RIGHT: {
                if (x < 700) {
                    x += speed;
                    setBounds(x, y, SIZE, SIZE);
                }
                break;
            }
        }
    }

    public boolean isDestroyed() {
        return destroyed;

    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
        setVisible(!destroyed);
    }

    public Bullet fire() {
        if (!destroyed) {
            Bullet bullet = new Bullet(x + 47, y - 10);
            return bullet;
        } else {
            return null;
        }
    }

    @Override
    public void setLocation(int x, int y) {
        setBounds(x, y, SIZE, SIZE);
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

}
