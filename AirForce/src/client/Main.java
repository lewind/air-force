/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import data.DataPacket;
import data.Room;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author admin
 */
public class Main extends javax.swing.JFrame implements KeyListener, ActionListener {

    private final int PORT = 7778;
    private String SERVER_IP = "127.0.0.1";
    private int hp = 15;
    private String playerName;
    private int numberOfEnemy = 0;
    private Socket socket;
    private int level = 1;
    private int levelThreshhold = 5;//5 máy bay địch xuất hiện thì tăng level
    private Timer timer = new Timer(5, null);
    private int countdown = 4;
    private Timer enemyExistTimer = new Timer(2000, null);
    private Timer countDownTimer = new Timer(1000, null);

    private int directionHost;
    private int directionGuest;

    private String role;//HOST||GUEST
    private Aircraft aircraftHost = new Aircraft(200, 500, 1);
    private Aircraft aircraftGuest = new Aircraft(500, 500, 2);

    private ArrayList<Enemy> listEnemy = new ArrayList<>();
    private ArrayList<Integer> listXEnemy = new ArrayList<>();
    private ArrayList<Bullet> listBullet = new ArrayList<>();
    private ObjectInputStream ois;
    private WaitingRoom wr;

    /**
     * Creates new form Main
     *
     * @param playerName
     * @throws java.io.IOException
     */
    public Main(String playerName, String ip) throws IOException {
        if (ip.length() > 0) {
            SERVER_IP = ip;
        }
        this.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("images/sky_background.png")))));
        initComponents();

        this.addKeyListener(this);
        countDownTimer.addActionListener(this);
        timer.addActionListener(this);
        enemyExistTimer.addActionListener(this);
        this.playerName = playerName;
        this.loginServer();
        addPlayer();
    }

    private void addPlayer() {
        aircraftGuest.setLocation(500, 500);
        aircraftHost.setLocation(200, 500);
        aircraftGuest.setDestroyed(false);
        aircraftHost.setDestroyed(false);
        this.add(aircraftHost);
        this.add(aircraftGuest);
    }

    private void listenServer() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {

                    ois = new ObjectInputStream(socket.getInputStream());
                    while (true) {
                        try {
                            DataPacket data = (DataPacket) ois.readObject();
                            switch (data.getAction()) {
                                case "RESPONSELISTROOM": {
                                    String listRoom = (String) data.getData();
                                    wr.setListRoom(listRoom);
                                    break;
                                }
                                case "GAME_START": {
                                    listEnemy.forEach((e) -> {
                                        e.setVisible(false);
                                    });
                                    listEnemy.removeAll(listEnemy);
                                    repaint();
                                    listBullet.removeAll(listBullet);
                                    repaint();
                                    txtCountDown.setVisible(true);
                                    countdown = 4;
                                    level = 1;
                                    addPlayer();
                                    directionGuest = 0;
                                    directionHost = 0;
                                    txtLevel.setText("Màn: " + level);
                                    levelThreshhold = 5;////5 máy bay địch xuất hiện thì tăng level
                                    hp = 15;
                                    wr.setVisible(false);
                                    setVisible(true);
                                    listXEnemy = (ArrayList<Integer>) data.getData();
                                    numberOfEnemy = listXEnemy.size();
                                    txtHP.setText("Máu: " + hp / 3);
                                    countDownTimer.start();
                                    repaint();
                                    break;
                                }
                                case "SET_ROLE": {
                                    role = (String) data.getData();
                                    break;
                                }
                                case "HOST_DIRECTION": {
                                    directionHost = (int) data.getData();
                                    break;
                                }
                                case "GUEST_DIRECTION": {
                                    directionGuest = (int) data.getData();
                                    break;
                                }
                                case "HOST_FIRE": {
                                    Bullet bullet = aircraftHost.fire();
                                    listBullet.add(bullet);
                                    add(bullet);
                                    repaint();
                                    break;
                                }
                                case "GUEST_FIRE": {
                                    Bullet bullet = aircraftGuest.fire();
                                    listBullet.add(bullet);
                                    add(bullet);
                                    repaint();
                                    break;
                                }
                                case "DUPLICATE": {
                                    JOptionPane.showMessageDialog(null, "Tên đã được sử dụng");
                                    System.exit(1);
                                    break;
                                }
                            }
                        } catch (ClassNotFoundException ex) {
                            ex.printStackTrace();
                        }
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    private void loginServer() {
        try {
            socket = new Socket(SERVER_IP, PORT);
            requestServer("LOGIN", playerName);
            wr = new WaitingRoom(socket, this);
            wr.setTitle(playerName);
            wr.setVisible(true);
            listenServer();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Can't connect to Server");
            System.exit(1);
        } finally {
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCountDown = new javax.swing.JLabel();
        txtHP = new javax.swing.JLabel();
        txtLevel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AirForce");
        setMinimumSize(new java.awt.Dimension(800, 600));
        setResizable(false);

        txtCountDown.setFont(new java.awt.Font("sansserif", 1, 100)); // NOI18N
        txtCountDown.setForeground(new java.awt.Color(255, 255, 255));
        txtCountDown.setText("3");

        txtHP.setFont(new java.awt.Font("sansserif", 1, 24)); // NOI18N
        txtHP.setForeground(new java.awt.Color(255, 255, 255));
        txtHP.setText("Máu");

        txtLevel.setFont(new java.awt.Font("sansserif", 1, 24)); // NOI18N
        txtLevel.setForeground(new java.awt.Color(255, 255, 255));
        txtLevel.setText("Level 1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(370, 370, 370)
                        .addComponent(txtCountDown)
                        .addGap(0, 368, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtHP)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtLevel)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHP)
                    .addComponent(txtLevel))
                .addGap(230, 230, 230)
                .addComponent(txtCountDown)
                .addContainerGap(205, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void keyTyped(KeyEvent e) {

    }

    public void requestServer(String actionType, String object) {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(socket.getOutputStream());
            DataPacket<String> data = new DataPacket<>(actionType, object);
            oos.writeObject(data);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //direction = e.getKeyCode();
        requestServer(role + "_ACTION", e.getKeyCode() + "");
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
//            Bullet bullet = aircraft.fire();
//            listBullet.add(bullet);
//            this.add(bullet);
            requestServer(role + "_FIRE", "");
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        requestServer(role + "_ACTION", "0");
    }

    public void checkCrash() {
        for (Bullet bullet : listBullet) {
            bullet.move();
        }
        int count = 0;
        for (Enemy enemy : listEnemy) {
            if (enemy.isDestroyed()) {
                count++;
            }
            if (count == numberOfEnemy) {
                timer.stop();
                enemyExistTimer.stop();
                JOptionPane.showMessageDialog(this, "Chúc mừng bạn đã chiến thắng");
                this.setVisible(false);
                wr.setVisible(true);
                break;
            }
            if (!enemy.isDestroyed() && enemy.isStarted()) {
                enemy.move();
                if (enemy.getY() >= 600) {
                    hp--;
                    txtHP.setText("Máu: " + hp / 3);
                    if (hp == 0) {
                        timer.stop();
                        enemyExistTimer.stop();
                        JOptionPane.showMessageDialog(this, "Bạn đã thua.");
                        this.setVisible(false);
                        wr.setVisible(true);
                        break;
                    }
                }
                for (Bullet bullet : listBullet) {
                    if (!bullet.isDestroyed()) {
                        //trường hợp bắn trúng mũi máy bay địch
                        boolean c1 = bullet.getY() <= enemy.getY() + 100;
                        boolean c2 = bullet.getX() >= enemy.getX() + 45;
                        boolean c3 = bullet.getX() <= enemy.getX() + 55;
                        //trường hợp bắn trúng cánh máy bay địch
                        boolean c4 = bullet.getY() <= enemy.getY() + 73;
                        boolean c5 = bullet.getX() >= enemy.getX();
                        boolean c6 = bullet.getX() <= enemy.getX() + 44;
                        boolean c7 = bullet.getX() >= enemy.getX() + 56;
                        boolean c8 = bullet.getX() <= enemy.getX() + 100;
                        if (c1 && c2 && c3) {
                            bullet.explode();
                            enemy.explode();
                            repaint();
                        }
                        if (c4 && ((c5 && c6) || (c7 && c8))) {
                            bullet.explode();
                            enemy.explode();
                            repaint();
                        }
                    }
                }
                //check trung may bay ta
                //trung host
                if (!aircraftHost.isDestroyed()) {

                    boolean c4 = aircraftHost.getY() <= enemy.getY() + 37;
                    boolean c5 = Math.abs(aircraftHost.getX() - enemy.getX()) < 100;

                    if (c4 && c5) {
                        aircraftHost.explode();
                        enemy.explode();
                        //this.remove(aircraftHost);

                        repaint();
                    }
                }
                if (!aircraftGuest.isDestroyed()) {
                    //trung guest

                    boolean c14 = aircraftGuest.getY() <= enemy.getY() + 37;
                    boolean c15 = Math.abs(aircraftGuest.getX() - enemy.getX()) < 100;

                    if (c14 && c15) {
                        aircraftGuest.explode();
                        enemy.explode();
                        //this.remove(aircraftGuest);
                        repaint();
                    }
                }
                if (aircraftHost.isDestroyed() && aircraftGuest.isDestroyed()) {
                    timer.stop();
                    enemyExistTimer.stop();
                    JOptionPane.showMessageDialog(this, "Bạn đã thua");
                    this.setVisible(false);
                    wr.setVisible(true);
                    break;
                }
            }

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == countDownTimer) {
            countdown--;
            if (countdown == 0) {
                txtCountDown.setVisible(false);
                timer.start();
                enemyExistTimer.start();
                countDownTimer.stop();
            }
            txtCountDown.setText(countdown + "");
        }
        if (e.getSource() == timer) {
            aircraftHost.move(directionHost);
            aircraftGuest.move(directionGuest);
            checkCrash();

        }
        if (e.getSource() == enemyExistTimer) {
            levelThreshhold--;
            if (levelThreshhold == 0) {
                level++;
                levelThreshhold = 5;
                aircraftHost.setSpeed(aircraftHost.getSpeed() + 1);
                aircraftGuest.setSpeed(aircraftGuest.getSpeed() + 1);
                txtLevel.setText("Level: " + level);
            }
            if (listXEnemy.size() > 0) {
                int x = listXEnemy.remove(listXEnemy.size() - 1);
                Enemy enemy = new Enemy(x, level);
                listEnemy.add(enemy);
                this.add(enemy);
                this.repaint();
                enemy.setStarted(true);
            } else {
//                JOptionPane.showMessageDialog(this, "Game Over");
                enemyExistTimer.stop();

            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel txtCountDown;
    private javax.swing.JLabel txtHP;
    private javax.swing.JLabel txtLevel;
    // End of variables declaration//GEN-END:variables
}
